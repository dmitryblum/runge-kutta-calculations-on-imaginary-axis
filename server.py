# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import cmath
import quadpy
from numpy import sqrt, exp, pi, zeros, geomspace, double
import numpy as np
#import matplotlib.pyplot as plt
import multiprocessing as mp
#import sys
#np.set_printoptions(threshold=sys.maxsize)
#import scipy.integrate as integrate
from functools import partial

from multiprocessing import Process, Queue

from multiprocessing.managers import BaseManager

#manager = BaseManager(address=('', 7299), authkey=b'5uPer_pAg$w0rk')
#server = manager.get_server()
#server.serve_forever()






gamma=0.077
aR, F_gam, aU,mult,h = 1.5, gamma, 23.0,4.0,0.01



Q1, Q2, RE1, RE2, W1, W2 = h, 6.0, -4.0, 4.0, -5.0, 5.0
RAN1, RAN2, IM1, IM2, isteps = -(2*Q2+RE2+73.0), 2*Q2+RE2+73.0, 15,1e-3, 35

resteps, ransteps = int(round((RE2-RE1)/h+1)), int(round((RAN2-RAN1)/h+1))
# qsteps, wsteps = int(round((Q2-Q1)/h+1)), int(round((W2-W1)/h+1))
# ransteps = int(round((RAN2-RAN1)/h+1))

z_val=np.linspace(RE1,RE2,resteps, endpoint=True, dtype=np.double)
# Z_val=np.linspace(RAN1,RAN2,ransteps, endpoint=True, dtype=np.double)
Im_val = np.geomspace(IM1,IM2, isteps, endpoint = False, dtype=np.double) 

# Q_val=np.linspace(Q1,Q2,qsteps, endpoint=True, dtype=np.double)
# W_val=np.linspace(W1,W2,wsteps, endpoint=True, dtype=np.double)
    
Surface1=np.zeros(ransteps,dtype=complex)
Surface2=np.zeros(ransteps,dtype=complex)
# import cmath
# import quadpy
# from numpy import sqrt, exp, pi
# import matplotlib.pyplot as plt
# import numpy as np
# import multiprocessing as mp
# import sys
# #np.set_printoptions(threshold=sys.maxsize)
# import scipy.integrate as integrate
# from functools import partial

# gamma, aR = 0.077, 1.5
# F_gam, aU = gamma, 23.0

# h = 0.01

# Q1, Q2 = h, 6.0
# RE1, RE2 = -4.0, 4.0
# W1, W2 = -5.0, 5.0
# RAN1, RAN2 = -(2*Q2+RE2+73.0), 2*Q2+RE2+73.0
# IM1, IM2, isteps = 15,1e-3, 35

# resteps, ransteps = int(round((RE2-RE1)/h+1)), int(round((RAN2-RAN1)/h+1))
# qsteps, wsteps = int(round((Q2-Q1)/h+1)), int(round((W2-W1)/h+1))

# z_val=np.linspace(RE1,RE2,resteps, endpoint=True, dtype=np.double)
# Z_val=np.linspace(RAN1,RAN2,ransteps, endpoint=True, dtype=np.double)
# Im_val = np.geomspace(IM1,IM2, isteps, endpoint = False, dtype=np.double) 

# Q_val=np.linspace(Q1,Q2,qsteps, endpoint=True, dtype=np.double)
# W_val=np.linspace(W1,W2,wsteps, endpoint=True, dtype=np.double)
    
# Surface1=np.zeros(ransteps,dtype=complex)
# Surface2=np.zeros(ransteps,dtype=complex)


def Ph(T,Shr,z):
    val, err=quadpy.quad(lambda q: h*Shr*q**3.0*exp(-q**2.0 / 2.0)*((1.0 + (exp(q / T) - 1.0)**-1) * (z-q + 1j*gamma/2.0 )**-1 + ((exp(q / T) - 1.0)**-1) * (z+q + 1j*gamma/2.0 )**-1), Q1, Q2)
    return val

def Fe(T,epsi,mu,z):
    val, err=quadpy.quad(lambda w: h*aR/pi*exp(-w**2.0 / 2.0)*(0.5*( ( - (exp((w-mu) / T) + 1.0)**-1) * (z-w + 1j*F_gam/2.0)**-1 + (   (exp((w-mu) / T) + 1.0)**-1) * (z+w -2.0*epsi-aU + 1j*F_gam/2.0)**-1 )-((exp((w-mu) / T) + 1.0)**-1 ) * (z+w -epsi + 1j*F_gam/2.0)**-1), W1, W2)
    return val


def Phonon(T, Shr): # C(z)
    func = partial(Ph, T, Shr)
    if __name__ == '__main__':
        p=m.Pool()
        ans = np.array(p.map(func, z_val))

    return ans-ans[int(round(RE2/h))]

def Fermion(T, epsi): # C(z)
    mu = epsi+5.0
    func1 = partial(Fe, T,epsi,mu)
    if __name__ == '__main__':
        p=m.Pool()
        ans = p.map(func1, z_val)
            
    shift = -ans[int(round(RE2/h))]
    arr = np.array(ans)
        
    return arr+shift

def Phonon_test(T, Shr, re, a1, a2): # dC(z)/dz
    ans = 0
    q=Q1
    while q < Q2:
        ans += -h*Shr*q**3.0*exp(-q**2.0 / 2.0)*( 
        (1.0 + (exp(q / T) - 1.0)**-1) * (re+a1-q + 1j*gamma/2.0 )**-2 + 
        ((exp(q / T) - 1.0)**-1) * (re+a1+q + 1j*gamma/2.0 )**-2 
        )
        q += h
    return ans

def Phonon_runge(T, Shr, re, a1, a2): # dC(z)/dz
    ans = 0
    q=Q1
    while q < Q2:
        ans += -h*Shr*q**3.0*exp(-q**2.0 / 2.0)*( 
        (1.0 + (exp(q / T) - 1.0)**-1) * (re+a1-q-(Surface1[int(round((RAN2+re-q)/h)) ]+ a2) + 1j*gamma/2.0 )**-2 + 
        ((exp(q / T) - 1.0)**-1) * (re+a1+q-(Surface1[int(round((RAN2+re+q)/h)) ]+ a2) + 1j*gamma/2.0 )**-2 
        )
        q += h
    return ans

def Fermion_runge(T, epsi, re, a1, a2): # dC(z)/dz
    ans = 0
    mu = epsi+5.0
    w=W1
    while w < W2:
        ans += -h*aR/pi*exp(-w**2.0 / 2.0)*(
        0.5*(
        ( - (exp((w-mu) / T) + 1.0)**-1) * (re+a1-w-(Surface1[int(round((RAN2+re-w)/h))]+ a2) + 1j*F_gam/2.0)**-2 +
        (   (exp((w-mu) / T) + 1.0)**-1) * (re+a1+w-(Surface1[int(round((RAN2+re+w-2.0*epsi-aU)/h))]+ a2) - 2.0*epsi-aU + 1j*F_gam/2.0)**-2
        )-((exp((w-mu) / T) + 1.0)**-1 ) * (re+a1+w-epsi-(Surface1[int(round((RAN2+re+w-epsi)/h))]+ a2) + 1j*F_gam/2.0)**-2
        )
        w += h
    return ans

def Case(case,im,hi,T,epsi,Shr,re):
    if case==0:   # Phonon 
        k1=Phonon_runge(T, Shr, re, im*1j, 0.0) 
        k2=Phonon_runge(T, Shr, re, (im-hi/2)*1j, - 1j*hi*k1 / 2.0)
        k3=Phonon_runge(T, Shr, re, (im-hi/2)*1j, - 1j*hi*k2 / 2.0)
        k4=Phonon_runge(T, Shr, re, (im-hi)*1j, - 1j*hi*k3)
        #print(Surface1[int((RAN2+re)/h)])
        ans = Surface1[int(round((RAN2+re)/h))] + (-hi*1j / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
        return ans
        #print(Surface2[int((RAN2+re)/h)])
        
    elif case==1:   # Fermion
        k1=Fermion_runge(T, epsi, re, 1j*im, 0.0) 
        k2=Fermion_runge(T, epsi, re, 1j*(im-hi / 2.0), - 1j*hi*k1 / 2.0)
        k3=Fermion_runge(T, epsi, re, 1j*(im-hi / 2.0), - 1j*hi*k2 / 2.0)
        k4=Fermion_runge(T, epsi, re, 1j*(im-hi), - 1j*hi*k3)
        ans = Surface1[int(round((RAN2+re)/h))] + (-hi*1j / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
        return ans

    elif case==2:   # Phonon-Fermion
        k1=Phonon_runge(T, Shr, re, 1j*im, 0.0) + Fermion_runge(T, epsi, re, 1j*im, 0.0) 
        k2=Phonon_runge(T, Shr, re, 1j*(im-hi / 2.0), - 1j*hi*k1 / 2.0) + Fermion_runge(T, epsi, re, 1j*(im-hi / 2.0), - 1j*hi*k1 / 2.0)
        k3=Phonon_runge(T, Shr, re, 1j*(im-hi / 2.0), - 1j*hi*k2 / 2.0) + Fermion_runge(T, epsi, re, 1j*(im-hi / 2.0), - 1j*hi*k2 / 2.0)
        k4=Phonon_runge(T, Shr, re, 1j*(im-hi), - 1j*hi*k3) + Fermion_runge(T, epsi, re, 1j*(im-hi), - 1j*hi*k3)
        ans = Surface1[int(round((RAN2+re)/h))] + (-hi*1j / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
        return ans
    
    elif case==3:   # Phonon test
        k1=Phonon_test(T, Shr, re, im*1j, 0.0) 
        k2=Phonon_test(T, Shr, re, (im-hi/2)*1j, - 1j*hi*k1 / 2.0)
        k3=Phonon_test(T, Shr, re, (im-hi/2)*1j, - 1j*hi*k2 / 2.0)
        k4=Phonon_test(T, Shr, re, (im-hi)*1j, - 1j*hi*k3)
        #print(Surface1[int((RAN2+re)/h)])
        ans = Surface1[int(round((RAN2+re)/h))] + (-hi*1j / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
        return ans
               
    else: print("Error, case should be a number 0-3, but it is", case)

def Runge(case,T,epsi, Shr):
    Surface0=np.zeros(int(round((RAN2-RE2)*2/h)),dtype=complex)
    Surface1=np.zeros(ransteps,dtype=complex)
    Surface2=np.zeros(resteps,dtype=complex)
    im=IM1
    while im > IM2:
#     for i in range(isteps-1):
#         im=Im_val[i]
#         im_next=Im_val[i+1]
#         hi=im-im_next
        hi=im/mult
#         z=RE1
#         while z < RE2:
        func2=partial(Case, case,im,hi,T,epsi,Shr)
        if __name__ == '__main__':
            p= m.Pool()
            Surface2=np.array(p.map(func2, z_val))
#         for z in z_val:
#             Surface2[int(round((RAN2+z)/h))]=Case(case,im,hi,T,epsi,Shr,z)
                
        Surface2+=-Surface2[int(round(RE2/h))]
        Surface3=np.insert(Surface0, int(round((RAN2-RE2)/h)),Surface2)
        Surface1=Surface3
    
    return Surface1[int(round((RAN2-RE2)/h)):int(round((RAN2+RE2)/h)+1)] # correct! Right one +1 brcause it icludes z=0 step


def S(SE, T, D): #SE means Self-Energy term (vector) D=Diff
    epsilon = 12.9
    mu2 = 480e-7
    Grad = 0.002
    omega0=830.0
    Gx = Grad+(200.0+T*2.5)*1e-3
    Gc = 0.1
    kappa = 0.6
    g1=0.13
    g2=g1**2
    ans=np.zeros(resteps)
    
    z=RE1
    while z < RE2:
#     for z in z_val:
        omegac = omega0-D
        omega = z+omega0
        Pc = omegac**2-omega**2-omega*Gc*1j
        Pp = omega0**2-omega**2-omega*Gx*1j
        ans[int(round((RE2+z)/h))] = np.real(
        np.abs(

            (4.0*pi*omega**2*mu2)/
            (sqrt(4.0*g2-(Gx-Gc)**2/4.0)*
                2.0*omegac*(omegac-omega-Gc*1j/2.0)*epsilon)
            )
        *
        np.abs(
            (sqrt(4.0*g2-(Gx-Gc)**2/4.0)*(omegac-omega-Gc*1j/2.0))/
         ((omega0-omega-Gx*1j/2.0 
            + SE[int(round((RE2+z)/h))]
            )*(omegac-omega-Gc*1j/2.0)-g2)
            )
        )
        z+=h
    
        
    maximum = max(ans)
    z=RE1
    while z < RE2:
        ans[int(round((z-RE1)/h))]=ans[int(round((z-RE1)/h))]/maximum
        z+=h
    return ans
  
def All():
    Case_val=[0,1,2]
    T_val=[4.0* 0.0862,20* 0.0862,40* 0.0862]
    E_val=[-2.5,0.0,2.5]
    Shr_val=[0.2,0.4,0.6]
    Diff_val=[-1,0,1,2]
    #Phonons
    ans=z_val
    for Shr in Shr_val:
        for T in T_val:
            Sim=Phonon(T, Shr)
            Run=Runge(0,T,0, Shr)
            ans=np.dstack((ans, Sim))
            #print(ans,Run)
            ans=np.dstack((ans, Run))
            for D in Diff_val:
                S_Sim=S(Sim,T,D)
                S_Run=S(Run,T,D)
                ans = np.dstack((ans,S_Sim))
                ans = np.dstack((ans,S_Run))
                
    #Fermions
    for epsi in E_val:
        for T in T_val:
            Sim=Fermion(T, epsi)
            Sur=Runge(1,T,epsi, 0.2)
            ans=np.dstack((ans, Sim))
            ans=np.dstack((ans, Run))
            for D in Diff_val:
                S_Sim=S(Sim,T,D)
                S_Run=S(Run,T,D)
                ans = np.dstack((ans,S_Sim))
                ans = np.dstack((ans,S_Run))
    #Phonons+fermions
    for epsi in E_val:
        for Shr in Shr_val:
            for T in T_val:
                Sim1=Phonon(T, Shr)
                Sim2=Fermion(T, epsi)
                Sim= Phonon(T, Shr)+Fermion(T, epsi)
                Run=Runge(2,T,epsi, Shr)
                ans=np.dstack((ans, Sim))
                ans=np.dstack((ans, Run))
                for D in Diff_val:
                    S_Sim=S(Sim,T,D)
                    S_Run=S(Run,T,D)
                    ans = np.dstack((ans,S_Sim))
                    ans = np.dstack((ans,S_Run))
    return ans


#m = mp.Manager()

queue = Queue()
class QueueManager(BaseManager): pass
QueueManager.register('get_queue', callable=lambda:queue)
m = QueueManager(address=('', 7299), authkey=b'5uPer_pAg$w0rk')


#p=Process(target=All, args=(queue,))
#p.start()
#p.join()

s = m.get_server()
s.serve_forever()

queue.put(All)
print('All task requests sent\n', end='')


queue.join()
print("All work completed")
#INFO=All()

#queue = m.get_queue()
#queue.put('hello')


inq = Queue()
outq = Queue()
class QueueManager(BaseManager): pass
QueueManager.register('get_job_queue', callable=lambda:inq)
QueueManager.register('get_result_queue', callable=lambda:outq)
m = QueueManager(address=('', 7299), authkey=b'5uPer_pAg$w0rk')
s = m.get_server()


##пример
for Shr in Shr_val:
    for T in T_val:
        inq.put( "ans=Phonon(%s,%s)" % (T,Shr) )


print("Queue is ready")
s.serve_forever()
