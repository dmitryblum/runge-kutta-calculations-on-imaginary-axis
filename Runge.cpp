# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.



#include <iostream>
#include <complex>
#include <fstream>
#include <vector>
#include <pthread.h>
#include <algorithm>
// #include <gmp.h>
#define  II << ' ' <<


pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
// static spinlock_t my_lock=SPIN_LOCK_UNLOCKED;

struct arg_struct {
    double T;
    double Shr;
    double im;
    double epsi;
    double hi;
    int type;
    
};

// Create imaginary type
typedef std::complex<double> dcomp;
// typedef std::vector<dcomp> cvec;
// typedef std::vector<double> dvec;


namespace workspace {

///////////////////////////////////////////////////////////////////////////////////////////////
//Constants
dcomp I = sqrt(dcomp(-1));
double Pi = 3.14159, gamma=0.077;
// gamma = 77e-3; //Shr = 0.2;

// double aR=7.5, F_gam=gamma, mu = 300.0*7.5, aU=30.0*7.5;//50 10 3.0 16.0
double aR=1.5, F_gam=gamma, aU=23.0;// aU=30.0*1.5;// epsi=830.0;//50 10 3.0 16.0 mu = 7.0*1e3,



double h = 0.01;//0.005 for spectrum 0.01 enough 0.1 errors
double hi;// = 0.1; 
double mult=4.0; //2.0 errors 4.0 minimum 10.0 a lot?

// std::vector<double> Q, RE;
double Q1 = 0.1,			Q2 = 6.0;
double RE1 = -4.0,			RE2 = 4.0;//Q2+range
double W1 = -5.0,			W2 = 5.0;
double RAN1 = -(2*Q2+RE2+73.0),	RAN2 = 2*Q2+RE2+73.0;//+10 +15
double IM1 = 15,			IM2 = 1e-3;
std::vector<dcomp> Surface1,Surface2;//,Surface_ans;
int flag=0;



///////////////////////////////////////////////////////////////////////////////////////////////
//Functions

std::vector<dcomp> P(double T, double Shr){
	std::vector<dcomp> ans((RE2-RE1)/h);
	for (double z=RE1; z<=RE2 ;z+=h ){
		dcomp Integ;
		for(double q=Q1; q<=Q2 ;q+=h ){
			Integ+= h*Shr*pow(q, 3.0)*exp(-pow(q, 2.0) / 2.0)*(
			(1.0 + pow(exp(q / T) - 1.0,-1)) / (z-q + I*gamma/2.0 ) +
			( pow((exp(q / T) - 1.0),-1)) / (z+q + I*gamma/2.0 ));
		};
		ans[int((RE2+z)/h)] = Integ;
		// ans.push_back(Integ);
	}
	dcomp shift = -ans[int((RE2)/h)-1];
	for (double re=RE1; re<=RE2 ;re+=h) { 
			ans[int((RE2+re)/h)] += shift;
	}

	return ans;
}

std::vector<dcomp> F(double T,double epsi){
	std::vector<dcomp> ans((RE2-RE1)/h);
	double mu = epsi+5.0;
	for (double z=RE1; z<=RE2 ;z+=h ){
		dcomp Integ=0;
		for(double w=W1; w<=W2 ;w+=h ){
			Integ+= h*aR/(Pi)*exp(-pow(w, 2.0) / 2.0)*(
			0.5*(
			( - pow(exp((w-mu) / T) + 1.0,-1)) * pow(z-w + I*F_gam/2.0 ,-1.0) +
			( pow((exp((w-mu) / T) + 1.0),-1) ) * pow(z+w -2.0*epsi-aU + I*F_gam/2.0,-1.0)
			)
			-
			( pow((exp((w-mu) / T) + 1.0),-1) ) * pow(z+w -epsi + I*F_gam/2.0,-1.0)
			);
		};
		ans[int((RE2+z)/h)] = Integ;
	}
	dcomp shift = -ans[int((RE2)/h)-1];
	for (double re=RE1; re<=RE2 ;re+=h) { 
			ans[int((RE2+re)/h)] += shift;
	}

	return ans;
}
dcomp PT(double T,double Shr, double re,dcomp a1,dcomp a2){
	dcomp ans=0;
	for(double q=Q1; q<=Q2 ;q+=h ){
		ans+= -h*Shr*pow(q, 3.0)*exp(-pow(q, 2.0) / 2.0)*(
		(1.0 + pow(exp(q / T) - 1.0,-1)) * pow(re+a1-q
			+ I*gamma/2.0 
			,-2.0) +
		( pow((exp(q / T) - 1.0),-1)) * pow(re+a1+q
			+ I*gamma/2.0 
			,-2.0)
		);
	};
	return ans;	
}

dcomp PR(double T,double Shr, double re,dcomp a1,dcomp a2){
	dcomp ans=0;
	
	for(double q=Q1; q<=Q2 ;q+=h ){
		
		ans+= -h*Shr*pow(q, 3.0)*exp(-pow(q, 2.0) / 2.0)*(
		(1.0 + pow(exp(q / T) - 1.0,-1)) * pow(re+a1-q
			- (Surface1[int((RAN2+re-q)/h)]+ a2)
			+ I*gamma/2.0 
			,-2.0) +
		( pow((exp(q / T) - 1.0),-1)) * pow(re+a1+q
			- (Surface1[int((RAN2+re+q)/h)]+ a2)
			+ I*gamma/2.0 
			,-2.0)
		);

	};
	return ans;	
}

dcomp FR(double T,double epsi,double re,dcomp a1,dcomp a2){//что-то не то, три части и emu apsilon не понятно
	dcomp ans;
	double mu = epsi+5.0;
	for(double w=W1; w<=W2 ;w+=h ){
		ans+= -h*aR/(Pi)*exp(-pow(w, 2.0) / 2.0)*(
		0.5*(
		(- pow(exp((w-mu) / T) + 1.0,-1)) * pow(re+a1-w
			- (Surface1[int((RAN2+re-w)/h)]+ a2)
			+ I*F_gam/2.0 
			// + I*5.0*gamma/2.0 
			,-2.0) +
			( pow((exp((w-mu) / T) + 1.0),-1) ) * pow(re+a1+w -2.0*epsi-aU
			- (Surface1[int((RAN2+re+w-2.0*epsi-aU)/h)]+ a2)
			+ I*F_gam/2.0
			// + I*5.0*gamma/2.0 //2-5
			,-2.0))	-
			( pow((exp((w-mu) / T) + 1.0),-1) ) * pow(re+a1+w -epsi
			- (Surface1[int((RAN2+re+w-epsi)/h)]+ a2)
			+ I*F_gam/2.0
			// + I*5.0*gamma/2.0 
			,-2.0)
		);
	};
	return ans;	
}


void* Party(void *arg) {
	struct arg_struct * arg_struct = (struct arg_struct *) arg;
	double T = arg_struct->T;
	double Shr = arg_struct->Shr;
	double im = arg_struct->im;
	double hi = arg_struct->hi;
	double epsi = arg_struct->epsi;
	int type = arg_struct->type;
	double lf;
	pthread_mutex_lock(&mutex);
	switch(flag) {
	case 0 : lf=0; flag++; break;
	case 1 : lf=1; flag++; break;
	case 2 : lf=2; flag++; break;
	case 3 : lf=3; flag++; break;
	}
	pthread_mutex_unlock(&mutex);
	switch(type) {
	case 0 : 
	for (double re=RE1+h*lf; re<=RE2 ;re+=h*4.0){
		dcomp k1=PR(T, Shr, re, I*im, 0.0); 
		dcomp k2=PR(T, Shr, re, I*(im-hi / 2.0), - I*hi*k1 / 2.0);
		dcomp k3=PR(T, Shr, re, I*(im-hi / 2.0), - I*hi*k2 / 2.0);
		dcomp k4=PR(T, Shr, re, I*(im-hi), - I*hi*k3);
		Surface2[int((RAN2+re)/h)] = Surface1[int((RAN2+re)/h)] + (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
		// std::cout << (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4) << '\n'; 
	}break;

	case 1 : 
	for (double re=RE1+h*lf; re<=RE2 ;re+=h*4.0){
		dcomp k1=FR(T, epsi, re, I*im, 0.0); 
		dcomp k2=FR(T, epsi, re, I*(im-hi / 2.0), - I*hi*k1 / 2.0);
		dcomp k3=FR(T, epsi, re, I*(im-hi / 2.0), - I*hi*k2 / 2.0);
		dcomp k4=FR(T, epsi, re, I*(im-hi), - I*hi*k3);
		Surface2[int((RAN2+re)/h)] = Surface1[int((RAN2+re)/h)] + (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
		// std::cout << (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4) << '\n';
	}break;

	case 2 : 
	for (double re=RE1+h*lf; re<=RE2 ;re+=h*4.0){
		dcomp k1=PR(T, Shr, re, I*im, 0.0) +
				 FR(T, epsi, re, I*im, 0.0); 
		dcomp k2=PR(T, Shr, re, I*(im-hi / 2.0), - I*hi*k1 / 2.0) +
				 FR(T, epsi, re, I*(im-hi / 2.0), - I*hi*k1 / 2.0);
		dcomp k3=PR(T, Shr, re, I*(im-hi / 2.0), - I*hi*k2 / 2.0) +
				 FR(T, epsi, re, I*(im-hi / 2.0), - I*hi*k2 / 2.0);
		dcomp k4=PR(T, Shr, re, I*(im-hi), - I*hi*k3) +
				 FR(T, epsi, re, I*(im-hi), - I*hi*k3);
		
		Surface2[int((RAN2+re)/h)] = Surface1[int((RAN2+re)/h)] + (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
		// std::cout << (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4) << '\n';
	}break;
	case 3 : 
	for (double re=RE1+h*lf; re<=RE2 ;re+=h*4.0){
		dcomp k1=PT(T, Shr, re, I*im, 0.0); 
		dcomp k2=PT(T, Shr, re, I*(im-hi / 2.0), - I*hi*k1 / 2.0);
		dcomp k3=PT(T, Shr, re, I*(im-hi / 2.0), - I*hi*k2 / 2.0);
		dcomp k4=PT(T, Shr, re, I*(im-hi), - I*hi*k3);
		Surface2[int((RAN2+re)/h)] = Surface1[int((RAN2+re)/h)] + (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
		// std::cout << (-hi*I / 6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4) << '\n';
	}break;

	}
	
	pthread_exit(0);
}



std::vector<dcomp> R(int type,double T,double epsi,double Shr){  
	// double hi;
	Surface1.clear();Surface2.clear();
	Surface1.resize((RAN2-RAN1)/h);Surface2.resize((RAN2-RAN1)/h);
	struct arg_struct arg;arg.T = T;arg.Shr = Shr;arg.epsi = epsi;arg.type = type;
	for (double im=IM1; im>IM2; im-=hi){ 
		hi=im/mult;		
		std::cout << im II hi << '\n';
		arg.im = im;arg.hi=hi;
		flag=0;
		pthread_t id[4];
		for (int i = 0; i <= 3; ++i){pthread_create(&id[i], NULL, Party, &arg);}
		for (int i = 0; i <= 3; ++i){pthread_join(id[i],NULL);}
		dcomp shift = -Surface2[int(RAN2/h)-1];// C(0)
		for (double re=RE1; re<=RE2 ;re+=h) { 
			Surface2[int((RAN2+re)/h)] += shift;
		}
		Surface1.swap(Surface2);
	}
	std::vector<dcomp> ans((RE2-RE1)/h);
	for (double re=RE1; re<=RE2 ;re+=h){
		ans[int((RE2+re)/h)] = Surface1[int((RAN2+re)/h)];
	}
	return ans;
}


std::vector<double> S(std::vector<dcomp> SE, double T, double D){
	
	dcomp epsilon = 12.9;//12.9;nothin'
	dcomp mu2 = 480e-7;//6.278e-14; more nothin'


	dcomp Grad = 0.002;//0.6;/////100miev
	dcomp omega0=830.0;///830mev
	dcomp Gx = Grad+(200.0+T*2.5)*1e-3;//0.6;/////100miev
	// dcomp Gx = 0.077;//0.6;/////100miev
	dcomp Gc = 0.1;//0.6;/////100miev


	dcomp kappa = 0.6;
	double g1=0.13;
	double g2=g1*g1;//0.04;////////0.13mev

	std::vector<double> ans((RE2-RE1)/h);
	
	for (double re=RE1; re<=RE2 ;re+=h){
		dcomp omegac = omega0-D;
		dcomp omega = re+omega0;
		dcomp Pc = pow(omegac,2.0)-pow(omega,2.0)-I*omega*Gc;
		dcomp Pp = pow(omega0,2.0)-pow(omega,2.0)-I*omega*Gx;
		ans[int((RE2+re)/h)] = std::real(
		std::norm(

			(4.0*Pi*pow(omega,2.0)*mu2)/
			(sqrt(4.0*g2-pow(Gx-Gc,2.0)/4.0)*
				2.0*omegac*(omegac-omega-I*Gc/2.0)*epsilon)
			)
		*
		std::norm(
			(sqrt(4.0*g2-pow(Gx-Gc,2.0)/4.0)*(omegac-omega-I*Gc/2.0))/
		 ((omega0-omega-I*Gx/2.0 
		 	+ SE[int((RE2+re)/h)]
		 	)*(omegac-omega-I*Gc/2.0)-g2)
			)
		);
	}
	// double normal=0.0;
	// for (double re=RE1; re<=RE2 ;re+=h){
	// 	normal+=ans[int((RE2+re)/h)]*h;
	// }
	// for (double re=RE1; re<=RE2 ;re+=h){
	// 	ans[int((RE2+re)/h)]=ans[int((RE2+re)/h)]/normal;
	// }
	double max = *max_element(std::begin(ans), std::end(ans));
	for (double re=RE1; re<=RE2 ;re+=h){
		ans[int((RE2+re)/h)]=ans[int((RE2+re)/h)]/max;
	}
	return ans;
}

std::vector<dcomp> plu(std::vector<dcomp> A1, std::vector<dcomp> A2){
Surface1.clear(); Surface1.resize((RE2-RE1)/h);
for (double re=RE1; re<=RE2 ;re+=h){
Surface1[int((RE2+re)/h)]=A1[int((RE2+re)/h)]+A2[int((RE2+re)/h)];
}
return Surface1;
}

};using namespace workspace;





int main()
{
// std::cout II "I started" II '\n' ;
// std::vector<dcomp> 

// PR4_2=R(0,4.0* 0.0862,0,0.2),
// // FR4_0=R(1,4.0* 0.0862,-5.0,0.2),


// PFR4_0_2=R(2,4.0* 0.0862, 0.0,0.2)
// // PFR40_0_2=R(2,40.0* 0.0862, 0.25,0.2),
// // PFR40_0_6=R(2,40.0* 0.0862, 0.25,0.6),

// // PFR4_2_2=R(2,4.0* 0.0862, 0.0,0.2),
// // PFR40_2_2=R(2,40.0* 0.0862, 0.0,0.2),
// // PFR40_2_6=R(2,40.0* 0.0862, 0.0,0.6)


// ;
// std::cout II "I finished" II '\n' ;


// std::vector<double>
// S1=S(PR4_2,10.0, -1.0),
// S2=S(PFR4_0_2,10.0, -1.0)
// // S11=S(SimC40,10.0, -1.0),
// // S12=S(SimC40,10.0, 0.0),
// // S13=S(SimC40,10.0, 1.0),
// // S14=S(SimC40,10.0, 2.0),

// // H11=S(HarC40,10.0, -1.0),
// // H12=S(HarC40,10.0, 0.0),
// // H13=S(HarC40,10.0, 1.0),
// // H14=S(HarC40,10.0, 2.0)
// ;





// std::ofstream data;	data.open("/home/t/2Program/Latex/test.txt");

// for (double z=RE1; z<=RE2 ;z+=h){
// 	int i=(RE2+z)/h;
// 	data << z II
// 	PFR4_0_2[i].real() II 
// 	// PFR40_0_2[i].real() II 
// 	// PFR40_0_6[i].real() II 

// 	// PFR4_2_2[i].real() II 
// 	// PFR40_2_2[i].real() II 
// 	// PFR40_2_6[i].real() II 

// 	PR4_2[i].real() II 
// 	S1[i] II S2[i] II
// 	// FR4_0[i].real() II
	
	
// '\n' ;
// }
// data.close();



///// C(z) ///////
std::vector<dcomp>


P4_2=P(4.0* 0.0862,0.2), P20_2=P(20.0* 0.0862,0.2), P40_2=P(40.0* 0.0862,0.2),
P4_4=P(4.0* 0.0862,0.4), P20_4=P(20.0* 0.0862,0.4), P40_4=P(40.0* 0.0862,0.4),
P4_6=P(4.0* 0.0862,0.6), P20_6=P(20.0* 0.0862,0.6), P40_6=P(40.0* 0.0862,0.6),
F4_0=F(4.0* 0.0862,-5.0), F20_0=F(20.0* 0.0862,-5.0), F40_0=F(40.0* 0.0862,-5.0),
F4_1=F(4.0* 0.0862,0.0), F20_1=F(20.0* 0.0862,0.0), F40_1=F(40.0* 0.0862,0.0),
F4_2=F(4.0* 0.0862,5.0), F20_2=F(20.0* 0.0862,5.0), F40_2=F(40.0* 0.0862,5.0),

PR4_2=R(0,4.0* 0.0862,0,0.2), PR20_2=R(0,20.0* 0.0862,0,0.2), PR40_2=R(0,40.0* 0.0862,0,0.2),
PR4_4=R(0,4.0* 0.0862,0,0.4), PR20_4=R(0,20.0* 0.0862,0,0.4), PR40_4=R(0,40.0* 0.0862,0,0.4),
PR4_6=R(0,4.0* 0.0862,0,0.6), PR20_6=R(0,20.0* 0.0862,0,0.6), PR40_6=R(0,40.0* 0.0862,0,0.6),
FR4_0=R(1,4.0* 0.0862,-2.5,0.2), FR20_0=R(1,20.0* 0.0862,-2.5,0.2), FR40_0=R(1,40.0* 0.0862,-2.5,0.2),
FR4_1=R(1,4.0* 0.0862,0.0,0.2), FR20_1=R(1,20.0* 0.0862,0.0,0.2), FR40_1=R(1,40.0* 0.0862,0.0,0.2),
FR4_2=R(1,4.0* 0.0862,2.5,0.2), FR20_2=R(1,20.0* 0.0862,2.5,0.2), FR40_2=R(1,40.0* 0.0862,2.5,0.2),
PFR4_0_2=R(2,4.0* 0.0862,-2.5,0.2), PFR20_0_2=R(2,20.0* 0.0862,-2.5,0.2), PFR40_0_2=R(2,40.0* 0.0862,-2.5,0.2),
PFR4_0_4=R(2,4.0* 0.0862,-2.5,0.4), PFR20_0_4=R(2,20.0* 0.0862,-2.5,0.4), PFR40_0_4=R(2,40.0* 0.0862,-2.5,0.4),
PFR4_0_6=R(2,4.0* 0.0862,-2.5,0.6), PFR20_0_6=R(2,20.0* 0.0862,-2.5,0.6), PFR40_0_6=R(2,40.0* 0.0862,-2.5,0.6),
PFR4_1_2=R(2,4.0* 0.0862,0.0,0.2), PFR20_1_2=R(2,20.0* 0.0862,0.0,0.2), PFR40_1_2=R(2,40.0* 0.0862,0.0,0.2),
PFR4_1_4=R(2,4.0* 0.0862,0.0,0.4), PFR20_1_4=R(2,20.0* 0.0862,0.0,0.4), PFR40_1_4=R(2,40.0* 0.0862,0.0,0.4),
PFR4_1_6=R(2,4.0* 0.0862,0.0,0.6), PFR20_1_6=R(2,20.0* 0.0862,0.0,0.6), PFR40_1_6=R(2,40.0* 0.0862,0.0,0.6),
PFR4_2_2=R(2,4.0* 0.0862,2.5,0.2), PFR20_2_2=R(2,20.0* 0.0862,2.5,0.2), PFR40_2_2=R(2,40.0* 0.0862,2.5,0.2),
PFR4_2_4=R(2,4.0* 0.0862,2.5,0.4), PFR20_2_4=R(2,20.0* 0.0862,2.5,0.4), PFR40_2_4=R(2,40.0* 0.0862,2.5,0.4),
PFR4_2_6=R(2,4.0* 0.0862,2.5,0.6), PFR20_2_6=R(2,20.0* 0.0862,2.5,0.6), PFR40_2_6=R(2,40.0* 0.0862,2.5,0.6)
;

std::vector<dcomp>
PF4_0_2=plu(P4_2,F4_0), PF20_0_2=plu(P20_2,F20_0), PF40_0_2=plu(P40_2,F40_0),
PF4_0_4=plu(P4_4,F4_0), PF20_0_4=plu(P20_4,F20_0), PF40_0_4=plu(P40_4,F40_0),
PF4_0_6=plu(P4_6,F4_0), PF20_0_6=plu(P20_6,F20_0), PF40_0_6=plu(P40_6,F40_0),

PF4_1_2=plu(P4_2,F4_1), PF20_1_2=plu(P20_2,F20_1), PF40_1_2=plu(P40_2,F40_1),
PF4_1_4=plu(P4_4,F4_1), PF20_1_4=plu(P20_4,F20_1), PF40_1_4=plu(P40_4,F40_1),
PF4_1_6=plu(P4_6,F4_1), PF20_1_6=plu(P20_6,F20_1), PF40_1_6=plu(P40_6,F40_1),

PF4_2_2=plu(P4_2,F4_2), PF20_2_2=plu(P20_2,F20_2), PF40_2_2=plu(P40_2,F40_2),
PF4_2_4=plu(P4_4,F4_2), PF20_2_4=plu(P20_4,F20_2), PF40_2_4=plu(P40_4,F40_2),
PF4_2_6=plu(P4_6,F4_2), PF20_2_6=plu(P20_6,F20_2), PF40_2_6=plu(P40_6,F40_2)
;

///////// Spectrum ///////
std::vector<double>

SP4_2_m1=S(P4_2,4.0,-1.0), SP20_2_m1=S(P20_2,20.0,-1.0), SP40_2_m1=S(P40_2,40.0,-1.0),
SP4_4_m1=S(P4_4,4.0,-1.0), SP20_4_m1=S(P20_4,20.0,-1.0), SP40_4_m1=S(P40_4,40.0,-1.0),
SP4_6_m1=S(P4_6,4.0,-1.0), SP20_6_m1=S(P20_6,20.0,-1.0), SP40_6_m1=S(P40_6,40.0,-1.0),
SP4_2_p0=S(P4_2,4.0, 0.0), SP20_2_p0=S(P20_2,20.0, 0.0), SP40_2_p0=S(P40_2,40.0, 0.0),
SP4_4_p0=S(P4_4,4.0, 0.0), SP20_4_p0=S(P20_4,20.0, 0.0), SP40_4_p0=S(P40_4,40.0, 0.0),
SP4_6_p0=S(P4_6,4.0, 0.0), SP20_6_p0=S(P20_6,20.0, 0.0), SP40_6_p0=S(P40_6,40.0, 0.0),
SP4_2_p1=S(P4_2,4.0, 1.0), SP20_2_p1=S(P20_2,20.0, 1.0), SP40_2_p1=S(P40_2,40.0, 1.0),
SP4_4_p1=S(P4_4,4.0, 1.0), SP20_4_p1=S(P20_4,20.0, 1.0), SP40_4_p1=S(P40_4,40.0, 1.0),
SP4_6_p1=S(P4_6,4.0, 1.0), SP20_6_p1=S(P20_6,20.0, 1.0), SP40_6_p1=S(P40_6,40.0, 1.0),
SP4_2_p2=S(P4_2,4.0, 2.0), SP20_2_p2=S(P20_2,20.0, 2.0), SP40_2_p2=S(P40_2,40.0, 2.0),
SP4_4_p2=S(P4_4,4.0, 2.0), SP20_4_p2=S(P20_4,20.0, 2.0), SP40_4_p2=S(P40_4,40.0, 2.0),
SP4_6_p2=S(P4_6,4.0, 2.0), SP20_6_p2=S(P20_6,20.0, 2.0), SP40_6_p2=S(P40_6,40.0, 2.0),
SF4_0_m1=S(F4_0,4.0,-1.0), SF20_0_m1=S(F20_0,20.0,-1.0), SF40_0_m1=S(F40_0,40.0,-1.0),
SF4_1_m1=S(F4_1,4.0,-1.0), SF20_1_m1=S(F20_1,20.0,-1.0), SF40_1_m1=S(F40_1,40.0,-1.0),
SF4_2_m1=S(F4_2,4.0,-1.0), SF20_2_m1=S(F20_2,20.0,-1.0), SF40_2_m1=S(F40_2,40.0,-1.0),
SF4_0_p0=S(F4_0,4.0, 0.0), SF20_0_p0=S(F20_0,20.0, 0.0), SF40_0_p0=S(F40_0,40.0, 0.0),
SF4_1_p0=S(F4_1,4.0, 0.0), SF20_1_p0=S(F20_1,20.0, 0.0), SF40_1_p0=S(F40_1,40.0, 0.0),
SF4_2_p0=S(F4_2,4.0, 0.0), SF20_2_p0=S(F20_2,20.0, 0.0), SF40_2_p0=S(F40_2,40.0, 0.0),
SF4_0_p1=S(F4_0,4.0, 1.0), SF20_0_p1=S(F20_0,20.0, 1.0), SF40_0_p1=S(F40_0,40.0, 1.0),
SF4_1_p1=S(F4_1,4.0, 1.0), SF20_1_p1=S(F20_1,20.0, 1.0), SF40_1_p1=S(F40_1,40.0, 1.0),
SF4_2_p1=S(F4_2,4.0, 1.0), SF20_2_p1=S(F20_2,20.0, 1.0), SF40_2_p1=S(F40_2,40.0, 1.0),
SF4_0_p2=S(F4_0,4.0, 2.0), SF20_0_p2=S(F20_0,20.0, 2.0), SF40_0_p2=S(F40_0,40.0, 2.0),
SF4_1_p2=S(F4_1,4.0, 2.0), SF20_1_p2=S(F20_1,20.0, 2.0), SF40_1_p2=S(F40_1,40.0, 2.0),
SF4_2_p2=S(F4_2,4.0, 2.0), SF20_2_p2=S(F20_2,20.0, 2.0), SF40_2_p2=S(F40_2,40.0, 2.0), 


SPF4_0_2_m1=S(PF4_0_2,4.0,-1.0), SPF20_0_2_m1=S(PF20_0_2,20.0,-1.0), SPF40_0_2_m1=S(PF40_0_2,40.0,-1.0),
SPF4_0_4_m1=S(PF4_0_4,4.0,-1.0), SPF20_0_4_m1=S(PF20_0_4,20.0,-1.0), SPF40_0_4_m1=S(PF40_0_4,40.0,-1.0),
SPF4_0_6_m1=S(PF4_0_6,4.0,-1.0), SPF20_0_6_m1=S(PF20_0_6,20.0,-1.0), SPF40_0_6_m1=S(PF40_0_6,40.0,-1.0),
SPF4_1_2_m1=S(PF4_1_2,4.0,-1.0), SPF20_1_2_m1=S(PF20_1_2,20.0,-1.0), SPF40_1_2_m1=S(PF40_1_2,40.0,-1.0),
SPF4_1_4_m1=S(PF4_1_4,4.0,-1.0), SPF20_1_4_m1=S(PF20_1_4,20.0,-1.0), SPF40_1_4_m1=S(PF40_1_4,40.0,-1.0),
SPF4_1_6_m1=S(PF4_1_6,4.0,-1.0), SPF20_1_6_m1=S(PF20_1_6,20.0,-1.0), SPF40_1_6_m1=S(PF40_1_6,40.0,-1.0),
SPF4_2_2_m1=S(PF4_2_2,4.0,-1.0), SPF20_2_2_m1=S(PF20_2_2,20.0,-1.0), SPF40_2_2_m1=S(PF40_2_2,40.0,-1.0),
SPF4_2_4_m1=S(PF4_2_4,4.0,-1.0), SPF20_2_4_m1=S(PF20_2_4,20.0,-1.0), SPF40_2_4_m1=S(PF40_2_4,40.0,-1.0),
SPF4_2_6_m1=S(PF4_2_6,4.0,-1.0), SPF20_2_6_m1=S(PF20_2_6,20.0,-1.0), SPF40_2_6_m1=S(PF40_2_6,40.0,-1.0),

SPF4_0_2_p0=S(PF4_0_2,4.0, 0.0), SPF20_0_2_p0=S(PF20_0_2,20.0, 0.0), SPF40_0_2_p0=S(PF40_0_2,40.0, 0.0),
SPF4_0_4_p0=S(PF4_0_4,4.0, 0.0), SPF20_0_4_p0=S(PF20_0_4,20.0, 0.0), SPF40_0_4_p0=S(PF40_0_4,40.0, 0.0),
SPF4_0_6_p0=S(PF4_0_6,4.0, 0.0), SPF20_0_6_p0=S(PF20_0_6,20.0, 0.0), SPF40_0_6_p0=S(PF40_0_6,40.0, 0.0),
SPF4_1_2_p0=S(PF4_1_2,4.0, 0.0), SPF20_1_2_p0=S(PF20_1_2,20.0, 0.0), SPF40_1_2_p0=S(PF40_1_2,40.0, 0.0),
SPF4_1_4_p0=S(PF4_1_4,4.0, 0.0), SPF20_1_4_p0=S(PF20_1_4,20.0, 0.0), SPF40_1_4_p0=S(PF40_1_4,40.0, 0.0),
SPF4_1_6_p0=S(PF4_1_6,4.0, 0.0), SPF20_1_6_p0=S(PF20_1_6,20.0, 0.0), SPF40_1_6_p0=S(PF40_1_6,40.0, 0.0),
SPF4_2_2_p0=S(PF4_2_2,4.0, 0.0), SPF20_2_2_p0=S(PF20_2_2,20.0, 0.0), SPF40_2_2_p0=S(PF40_2_2,40.0, 0.0),
SPF4_2_4_p0=S(PF4_2_4,4.0, 0.0), SPF20_2_4_p0=S(PF20_2_4,20.0, 0.0), SPF40_2_4_p0=S(PF40_2_4,40.0, 0.0),
SPF4_2_6_p0=S(PF4_2_6,4.0, 0.0), SPF20_2_6_p0=S(PF20_2_6,20.0, 0.0), SPF40_2_6_p0=S(PF40_2_6,40.0, 0.0),

SPF4_0_2_p1=S(PF4_0_2,4.0, 1.0), SPF20_0_2_p1=S(PF20_0_2,20.0, 1.0), SPF40_0_2_p1=S(PF40_0_2,40.0, 1.0),
SPF4_0_4_p1=S(PF4_0_4,4.0, 1.0), SPF20_0_4_p1=S(PF20_0_4,20.0, 1.0), SPF40_0_4_p1=S(PF40_0_4,40.0, 1.0),
SPF4_0_6_p1=S(PF4_0_6,4.0, 1.0), SPF20_0_6_p1=S(PF20_0_6,20.0, 1.0), SPF40_0_6_p1=S(PF40_0_6,40.0, 1.0),
SPF4_1_2_p1=S(PF4_1_2,4.0, 1.0), SPF20_1_2_p1=S(PF20_1_2,20.0, 1.0), SPF40_1_2_p1=S(PF40_1_2,40.0, 1.0),
SPF4_1_4_p1=S(PF4_1_4,4.0, 1.0), SPF20_1_4_p1=S(PF20_1_4,20.0, 1.0), SPF40_1_4_p1=S(PF40_1_4,40.0, 1.0),
SPF4_1_6_p1=S(PF4_1_6,4.0, 1.0), SPF20_1_6_p1=S(PF20_1_6,20.0, 1.0), SPF40_1_6_p1=S(PF40_1_6,40.0, 1.0),
SPF4_2_2_p1=S(PF4_2_2,4.0, 1.0), SPF20_2_2_p1=S(PF20_2_2,20.0, 1.0), SPF40_2_2_p1=S(PF40_2_2,40.0, 1.0),
SPF4_2_4_p1=S(PF4_2_4,4.0, 1.0), SPF20_2_4_p1=S(PF20_2_4,20.0, 1.0), SPF40_2_4_p1=S(PF40_2_4,40.0, 1.0),
SPF4_2_6_p1=S(PF4_2_6,4.0, 1.0), SPF20_2_6_p1=S(PF20_2_6,20.0, 1.0), SPF40_2_6_p1=S(PF40_2_6,40.0, 1.0),

SPF4_0_2_p2=S(PF4_0_2,4.0, 2.0), SPF20_0_2_p2=S(PF20_0_2,20.0, 2.0), SPF40_0_2_p2=S(PF40_0_2,40.0, 2.0),
SPF4_0_4_p2=S(PF4_0_4,4.0, 2.0), SPF20_0_4_p2=S(PF20_0_4,20.0, 2.0), SPF40_0_4_p2=S(PF40_0_4,40.0, 2.0),
SPF4_0_6_p2=S(PF4_0_6,4.0, 2.0), SPF20_0_6_p2=S(PF20_0_6,20.0, 2.0), SPF40_0_6_p2=S(PF40_0_6,40.0, 2.0),
SPF4_1_2_p2=S(PF4_1_2,4.0, 2.0), SPF20_1_2_p2=S(PF20_1_2,20.0, 2.0), SPF40_1_2_p2=S(PF40_1_2,40.0, 2.0),
SPF4_1_4_p2=S(PF4_1_4,4.0, 2.0), SPF20_1_4_p2=S(PF20_1_4,20.0, 2.0), SPF40_1_4_p2=S(PF40_1_4,40.0, 2.0),
SPF4_1_6_p2=S(PF4_1_6,4.0, 2.0), SPF20_1_6_p2=S(PF20_1_6,20.0, 2.0), SPF40_1_6_p2=S(PF40_1_6,40.0, 2.0),
SPF4_2_2_p2=S(PF4_2_2,4.0, 2.0), SPF20_2_2_p2=S(PF20_2_2,20.0, 2.0), SPF40_2_2_p2=S(PF40_2_2,40.0, 2.0),
SPF4_2_4_p2=S(PF4_2_4,4.0, 2.0), SPF20_2_4_p2=S(PF20_2_4,20.0, 2.0), SPF40_2_4_p2=S(PF40_2_4,40.0, 2.0),
SPF4_2_6_p2=S(PF4_2_6,4.0, 2.0), SPF20_2_6_p2=S(PF20_2_6,20.0, 2.0), SPF40_2_6_p2=S(PF40_2_6,40.0, 2.0),



SPR4_2_m1=S(PR4_2,4.0,-1.0), SPR20_2_m1=S(PR20_2,20.0,-1.0), SPR40_2_m1=S(PR40_2,40.0,-1.0),
SPR4_4_m1=S(PR4_4,4.0,-1.0), SPR20_4_m1=S(PR20_4,20.0,-1.0), SPR40_4_m1=S(PR40_4,40.0,-1.0),
SPR4_6_m1=S(PR4_6,4.0,-1.0), SPR20_6_m1=S(PR20_6,20.0,-1.0), SPR40_6_m1=S(PR40_6,40.0,-1.0),
SPR4_2_p0=S(PR4_2,4.0, 0.0), SPR20_2_p0=S(PR20_2,20.0, 0.0), SPR40_2_p0=S(PR40_2,40.0, 0.0),
SPR4_4_p0=S(PR4_4,4.0, 0.0), SPR20_4_p0=S(PR20_4,20.0, 0.0), SPR40_4_p0=S(PR40_4,40.0, 0.0),
SPR4_6_p0=S(PR4_6,4.0, 0.0), SPR20_6_p0=S(PR20_6,20.0, 0.0), SPR40_6_p0=S(PR40_6,40.0, 0.0),
SPR4_2_p1=S(PR4_2,4.0, 1.0), SPR20_2_p1=S(PR20_2,20.0, 1.0), SPR40_2_p1=S(PR40_2,40.0, 1.0),
SPR4_4_p1=S(PR4_4,4.0, 1.0), SPR20_4_p1=S(PR20_4,20.0, 1.0), SPR40_4_p1=S(PR40_4,40.0, 1.0),
SPR4_6_p1=S(PR4_6,4.0, 1.0), SPR20_6_p1=S(PR20_6,20.0, 1.0), SPR40_6_p1=S(PR40_6,40.0, 1.0),
SPR4_2_p2=S(PR4_2,4.0, 2.0), SPR20_2_p2=S(PR20_2,20.0, 2.0), SPR40_2_p2=S(PR40_2,40.0, 2.0),
SPR4_4_p2=S(PR4_4,4.0, 2.0), SPR20_4_p2=S(PR20_4,20.0, 2.0), SPR40_4_p2=S(PR40_4,40.0, 2.0),
SPR4_6_p2=S(PR4_6,4.0, 2.0), SPR20_6_p2=S(PR20_6,20.0, 2.0), SPR40_6_p2=S(PR40_6,40.0, 2.0),
SFR4_0_m1=S(FR4_0,4.0,-1.0), SFR20_0_m1=S(FR20_0,20.0,-1.0), SFR40_0_m1=S(FR40_0,40.0,-1.0),
SFR4_1_m1=S(FR4_1,4.0,-1.0), SFR20_1_m1=S(FR20_1,20.0,-1.0), SFR40_1_m1=S(FR40_1,40.0,-1.0),
SFR4_2_m1=S(FR4_2,4.0,-1.0), SFR20_2_m1=S(FR20_2,20.0,-1.0), SFR40_2_m1=S(FR40_2,40.0,-1.0),
SFR4_0_p0=S(FR4_0,4.0, 0.0), SFR20_0_p0=S(FR20_0,20.0, 0.0), SFR40_0_p0=S(FR40_0,40.0, 0.0),
SFR4_1_p0=S(FR4_1,4.0, 0.0), SFR20_1_p0=S(FR20_1,20.0, 0.0), SFR40_1_p0=S(FR40_1,40.0, 0.0),
SFR4_2_p0=S(FR4_2,4.0, 0.0), SFR20_2_p0=S(FR20_2,20.0, 0.0), SFR40_2_p0=S(FR40_2,40.0, 0.0),
SFR4_0_p1=S(FR4_0,4.0, 1.0), SFR20_0_p1=S(FR20_0,20.0, 1.0), SFR40_0_p1=S(FR40_0,40.0, 1.0),
SFR4_1_p1=S(FR4_1,4.0, 1.0), SFR20_1_p1=S(FR20_1,20.0, 1.0), SFR40_1_p1=S(FR40_1,40.0, 1.0),
SFR4_2_p1=S(FR4_2,4.0, 1.0), SFR20_2_p1=S(FR20_2,20.0, 1.0), SFR40_2_p1=S(FR40_2,40.0, 1.0),
SFR4_0_p2=S(FR4_0,4.0, 2.0), SFR20_0_p2=S(FR20_0,20.0, 2.0), SFR40_0_p2=S(FR40_0,40.0, 2.0),
SFR4_1_p2=S(FR4_1,4.0, 2.0), SFR20_1_p2=S(FR20_1,20.0, 2.0), SFR40_1_p2=S(FR40_1,40.0, 2.0),
SFR4_2_p2=S(FR4_2,4.0, 2.0), SFR20_2_p2=S(FR20_2,20.0, 2.0), SFR40_2_p2=S(FR40_2,40.0, 2.0),
SPFR4_0_2_m1=S(PFR4_0_2,4.0,-1.0), SPFR20_0_2_m1=S(PFR20_0_2,20.0,-1.0), SPFR40_0_2_m1=S(PFR40_0_2,40.0,-1.0),
SPFR4_1_2_m1=S(PFR4_1_2,4.0,-1.0), SPFR20_1_2_m1=S(PFR20_1_2,20.0,-1.0), SPFR40_1_2_m1=S(PFR40_1_2,40.0,-1.0),
SPFR4_2_2_m1=S(PFR4_2_2,4.0,-1.0), SPFR20_2_2_m1=S(PFR20_2_2,20.0,-1.0), SPFR40_2_2_m1=S(PFR40_2_2,40.0,-1.0),
SPFR4_0_4_m1=S(PFR4_0_4,4.0,-1.0), SPFR20_0_4_m1=S(PFR20_0_4,20.0,-1.0), SPFR40_0_4_m1=S(PFR40_0_4,40.0,-1.0),
SPFR4_1_4_m1=S(PFR4_1_4,4.0,-1.0), SPFR20_1_4_m1=S(PFR20_1_4,20.0,-1.0), SPFR40_1_4_m1=S(PFR40_1_4,40.0,-1.0),
SPFR4_2_4_m1=S(PFR4_2_4,4.0,-1.0), SPFR20_2_4_m1=S(PFR20_2_4,20.0,-1.0), SPFR40_2_4_m1=S(PFR40_2_4,40.0,-1.0),
SPFR4_0_6_m1=S(PFR4_0_6,4.0,-1.0), SPFR20_0_6_m1=S(PFR20_0_6,20.0,-1.0), SPFR40_0_6_m1=S(PFR40_0_6,40.0,-1.0),
SPFR4_1_6_m1=S(PFR4_1_6,4.0,-1.0), SPFR20_1_6_m1=S(PFR20_1_6,20.0,-1.0), SPFR40_1_6_m1=S(PFR40_1_6,40.0,-1.0),
SPFR4_2_6_m1=S(PFR4_2_6,4.0,-1.0), SPFR20_2_6_m1=S(PFR20_2_6,20.0,-1.0), SPFR40_2_6_m1=S(PFR40_2_6,40.0,-1.0),
SPFR4_0_2_p0=S(PFR4_0_2,4.0, 0.0), SPFR20_0_2_p0=S(PFR20_0_2,20.0, 0.0), SPFR40_0_2_p0=S(PFR40_0_2,40.0, 0.0),
SPFR4_1_2_p0=S(PFR4_1_2,4.0, 0.0), SPFR20_1_2_p0=S(PFR20_1_2,20.0, 0.0), SPFR40_1_2_p0=S(PFR40_1_2,40.0, 0.0),
SPFR4_2_2_p0=S(PFR4_2_2,4.0, 0.0), SPFR20_2_2_p0=S(PFR20_2_2,20.0, 0.0), SPFR40_2_2_p0=S(PFR40_2_2,40.0, 0.0),
SPFR4_0_4_p0=S(PFR4_0_4,4.0, 0.0), SPFR20_0_4_p0=S(PFR20_0_4,20.0, 0.0), SPFR40_0_4_p0=S(PFR40_0_4,40.0, 0.0),
SPFR4_1_4_p0=S(PFR4_1_4,4.0, 0.0), SPFR20_1_4_p0=S(PFR20_1_4,20.0, 0.0), SPFR40_1_4_p0=S(PFR40_1_4,40.0, 0.0),
SPFR4_2_4_p0=S(PFR4_2_4,4.0, 0.0), SPFR20_2_4_p0=S(PFR20_2_4,20.0, 0.0), SPFR40_2_4_p0=S(PFR40_2_4,40.0, 0.0),
SPFR4_0_6_p0=S(PFR4_0_6,4.0, 0.0), SPFR20_0_6_p0=S(PFR20_0_6,20.0, 0.0), SPFR40_0_6_p0=S(PFR40_0_6,40.0, 0.0),
SPFR4_1_6_p0=S(PFR4_1_6,4.0, 0.0), SPFR20_1_6_p0=S(PFR20_1_6,20.0, 0.0), SPFR40_1_6_p0=S(PFR40_1_6,40.0, 0.0),
SPFR4_2_6_p0=S(PFR4_2_6,4.0, 0.0), SPFR20_2_6_p0=S(PFR20_2_6,20.0, 0.0), SPFR40_2_6_p0=S(PFR40_2_6,40.0, 0.0),
SPFR4_0_2_p1=S(PFR4_0_2,4.0, 1.0), SPFR20_0_2_p1=S(PFR20_0_2,20.0, 1.0), SPFR40_0_2_p1=S(PFR40_0_2,40.0, 1.0),
SPFR4_1_2_p1=S(PFR4_1_2,4.0, 1.0), SPFR20_1_2_p1=S(PFR20_1_2,20.0, 1.0), SPFR40_1_2_p1=S(PFR40_1_2,40.0, 1.0),
SPFR4_2_2_p1=S(PFR4_2_2,4.0, 1.0), SPFR20_2_2_p1=S(PFR20_2_2,20.0, 1.0), SPFR40_2_2_p1=S(PFR40_2_2,40.0, 1.0),
SPFR4_0_4_p1=S(PFR4_0_4,4.0, 1.0), SPFR20_0_4_p1=S(PFR20_0_4,20.0, 1.0), SPFR40_0_4_p1=S(PFR40_0_4,40.0, 1.0),
SPFR4_1_4_p1=S(PFR4_1_4,4.0, 1.0), SPFR20_1_4_p1=S(PFR20_1_4,20.0, 1.0), SPFR40_1_4_p1=S(PFR40_1_4,40.0, 1.0),
SPFR4_2_4_p1=S(PFR4_2_4,4.0, 1.0), SPFR20_2_4_p1=S(PFR20_2_4,20.0, 1.0), SPFR40_2_4_p1=S(PFR40_2_4,40.0, 1.0),
SPFR4_0_6_p1=S(PFR4_0_6,4.0, 1.0), SPFR20_0_6_p1=S(PFR20_0_6,20.0, 1.0), SPFR40_0_6_p1=S(PFR40_0_6,40.0, 1.0),
SPFR4_1_6_p1=S(PFR4_1_6,4.0, 1.0), SPFR20_1_6_p1=S(PFR20_1_6,20.0, 1.0), SPFR40_1_6_p1=S(PFR40_1_6,40.0, 1.0),
SPFR4_2_6_p1=S(PFR4_2_6,4.0, 1.0), SPFR20_2_6_p1=S(PFR20_2_6,20.0, 1.0), SPFR40_2_6_p1=S(PFR40_2_6,40.0, 1.0),
SPFR4_0_2_p2=S(PFR4_0_2,4.0, 2.0), SPFR20_0_2_p2=S(PFR20_0_2,20.0, 2.0), SPFR40_0_2_p2=S(PFR40_0_2,40.0, 2.0),
SPFR4_1_2_p2=S(PFR4_1_2,4.0, 2.0), SPFR20_1_2_p2=S(PFR20_1_2,20.0, 2.0), SPFR40_1_2_p2=S(PFR40_1_2,40.0, 2.0),
SPFR4_2_2_p2=S(PFR4_2_2,4.0, 2.0), SPFR20_2_2_p2=S(PFR20_2_2,20.0, 2.0), SPFR40_2_2_p2=S(PFR40_2_2,40.0, 2.0),
SPFR4_0_4_p2=S(PFR4_0_4,4.0, 2.0), SPFR20_0_4_p2=S(PFR20_0_4,20.0, 2.0), SPFR40_0_4_p2=S(PFR40_0_4,40.0, 2.0),
SPFR4_1_4_p2=S(PFR4_1_4,4.0, 2.0), SPFR20_1_4_p2=S(PFR20_1_4,20.0, 2.0), SPFR40_1_4_p2=S(PFR40_1_4,40.0, 2.0),
SPFR4_2_4_p2=S(PFR4_2_4,4.0, 2.0), SPFR20_2_4_p2=S(PFR20_2_4,20.0, 2.0), SPFR40_2_4_p2=S(PFR40_2_4,40.0, 2.0),
SPFR4_0_6_p2=S(PFR4_0_6,4.0, 2.0), SPFR20_0_6_p2=S(PFR20_0_6,20.0, 2.0), SPFR40_0_6_p2=S(PFR40_0_6,40.0, 2.0),
SPFR4_1_6_p2=S(PFR4_1_6,4.0, 2.0), SPFR20_1_6_p2=S(PFR20_1_6,20.0, 2.0), SPFR40_1_6_p2=S(PFR40_1_6,40.0, 2.0),
SPFR4_2_6_p2=S(PFR4_2_6,4.0, 2.0), SPFR20_2_6_p2=S(PFR20_2_6,20.0, 2.0), SPFR40_2_6_p2=S(PFR40_2_6,40.0, 2.0)
;





std::ofstream data;	data.open("/home/t/2Program/Latex/data.txt");

for (double z=RE1; z<=RE2 ;z+=h){
	int i=(RE2+z)/h;
	data << z II
	
	

/////////// C(z) //////
// Simple Phonon 
// Shr\T
/*2-3-4*/		P4_2[i].real() II P20_2[i].real() II P40_2[i].real() II
/*5-6-7*/		P4_4[i].real() II P20_4[i].real() II P40_4[i].real() II
/*8-9-10*/		P4_6[i].real() II P20_6[i].real() II P40_6[i].real() II
//Simple Fermion 
//epsi\T
/*11-12-13*/	F4_0[i].real() II F20_0[i].real() II F40_0[i].real() II
/*14-15-16*/	F4_1[i].real() II F20_1[i].real() II F40_1[i].real() II
/*17-18-19*/	F4_2[i].real() II F20_2[i].real() II F40_2[i].real() II
// r Phonon 
// Shr\T
/*20-21-22*/	PR4_2[i].real() II PR20_2[i].real() II PR40_2[i].real() II
/*23-24-25*/	PR4_4[i].real() II PR20_4[i].real() II PR40_4[i].real() II
/*26-27-!28!*/	PR4_6[i].real() II PR20_6[i].real() II PR40_6[i].real() II
// r Fermion 
// epsi\T
/*!29!-30-31*/	FR4_0[i].real() II FR20_0[i].real() II FR40_0[i].real() II
/*32-33-34*/	FR4_1[i].real() II FR20_1[i].real() II FR40_1[i].real() II
/*35-36-37*/	FR4_2[i].real() II FR20_2[i].real() II FR40_2[i].real() II
// r Phonon + Fermion 
//Shr\T  epsi=0
/*38-39-40*/	PFR4_0_2[i].real() II PFR20_0_2[i].real() II PFR40_0_2[i].real() II
/*41-42-!43!*/	PFR4_0_4[i].real() II PFR20_0_4[i].real() II PFR40_0_4[i].real() II
/*44-45-!46!*/	PFR4_0_6[i].real() II PFR20_0_6[i].real() II PFR40_0_6[i].real() II
//Shr\T  epsi=1
/*47-48-49*/	PFR4_1_2[i].real() II PFR20_1_2[i].real() II PFR40_1_2[i].real() II
/*50-51-!52!*/	PFR4_1_4[i].real() II PFR20_1_4[i].real() II PFR40_1_4[i].real() II
/*53-54-!55!*/	PFR4_1_6[i].real() II PFR20_1_6[i].real() II PFR40_1_6[i].real() II
//Shr\T  epsi=2
/*56-57-58*/	PFR4_2_2[i].real() II PFR20_2_2[i].real() II PFR40_2_2[i].real() II
/*59-60-!61!*/	PFR4_2_4[i].real() II PFR20_2_4[i].real() II PFR40_2_4[i].real() II
/*62-63-!64!*/	PFR4_2_6[i].real() II PFR20_2_6[i].real() II PFR40_2_6[i].real() II
/////// Spectrum /////////
// Phonons simple 
/*65-66-67*/	SP4_2_m1[i] II SP20_2_m1[i] II SP40_2_m1[i] II
/*68-69-70*/	SP4_4_m1[i] II SP20_4_m1[i] II SP40_4_m1[i] II
/*71-72-73*/	SP4_6_m1[i] II SP20_6_m1[i] II SP40_6_m1[i] II
/*74-75-76*/	SP4_2_p0[i] II SP20_2_p0[i] II SP40_2_p0[i] II
/*77-78-79*/	SP4_4_p0[i] II SP20_4_p0[i] II SP40_4_p0[i] II
/*80-81-82*/	SP4_6_p0[i] II SP20_6_p0[i] II SP40_6_p0[i] II
/*83-84-85*/	SP4_2_p1[i] II SP20_2_p1[i] II SP40_2_p1[i] II
/*86-87-88*/	SP4_4_p1[i] II SP20_4_p1[i] II SP40_4_p1[i] II
/*89-90-91*/	SP4_6_p1[i] II SP20_6_p1[i] II SP40_6_p1[i] II
/*92-93-94*/	SP4_2_p2[i] II SP20_2_p2[i] II SP40_2_p2[i] II 
/*95-96-97*/	SP4_4_p2[i] II SP20_4_p2[i] II SP40_4_p2[i] II
/*98-99-100*/	SP4_6_p2[i] II SP20_6_p2[i] II SP40_6_p2[i] II

// Phonons+Fermions(1) simple // 
/*101-102-103*/	SPF4_0_2_m1[i] II SPF20_0_2_m1[i] II SPF40_0_2_m1[i] II
/*104-105-106*/	SPF4_0_4_m1[i] II SPF20_0_4_m1[i] II SPF40_0_4_m1[i] II
/*107-108-109*/	SPF4_0_6_m1[i] II SPF20_0_6_m1[i] II SPF40_0_6_m1[i] II
/*110-111-112*/	SPF4_0_2_p0[i] II SPF20_0_2_p0[i] II SPF40_0_2_p0[i] II
/*113-114-115*/	SPF4_0_4_p0[i] II SPF20_0_4_p0[i] II SPF40_0_4_p0[i] II
/*116-117-118*/	SPF4_0_6_p0[i] II SPF20_0_6_p0[i] II SPF40_0_6_p0[i] II
/*119-120-121*/	SPF4_0_2_p1[i] II SPF20_0_2_p1[i] II SPF40_0_2_p1[i] II
/*122-123-124*/	SPF4_0_4_p1[i] II SPF20_0_4_p1[i] II SPF40_0_4_p1[i] II
/*125-126-127*/	SPF4_0_6_p1[i] II SPF20_0_6_p1[i] II SPF40_0_6_p1[i] II
/*128-129-130*/	SPF4_0_2_p2[i] II SPF20_0_2_p2[i] II SPF40_0_2_p2[i] II
/*131-132-133*/	SPF4_0_4_p2[i] II SPF20_0_4_p2[i] II SPF40_0_4_p2[i] II
/*134-135-136*/	SPF4_0_6_p2[i] II SPF20_0_6_p2[i] II SPF40_0_6_p2[i] II

/*137-138-139*/	SPF4_1_2_m1[i] II SPF20_1_2_m1[i] II SPF40_1_2_m1[i] II
/*140-141-142*/	SPF4_1_4_m1[i] II SPF20_1_4_m1[i] II SPF40_1_4_m1[i] II
/*143-144-145*/	SPF4_1_6_m1[i] II SPF20_1_6_m1[i] II SPF40_1_6_m1[i] II
/*146-147-148*/	SPF4_1_2_p0[i] II SPF20_1_2_p0[i] II SPF40_1_2_p0[i] II
/*149-150-151*/	SPF4_1_4_p0[i] II SPF20_1_4_p0[i] II SPF40_1_4_p0[i] II
/*152-153-154*/	SPF4_1_6_p0[i] II SPF20_1_6_p0[i] II SPF40_1_6_p0[i] II
/*155-156-157*/	SPF4_1_2_p1[i] II SPF20_1_2_p1[i] II SPF40_1_2_p1[i] II
/*158-159-160*/	SPF4_1_4_p1[i] II SPF20_1_4_p1[i] II SPF40_1_4_p1[i] II
/*161-162-163*/	SPF4_1_6_p1[i] II SPF20_1_6_p1[i] II SPF40_1_6_p1[i] II
/*164-165-166*/	SPF4_1_2_p2[i] II SPF20_1_2_p2[i] II SPF40_1_2_p2[i] II
/*167-168-169*/	SPF4_1_4_p2[i] II SPF20_1_4_p2[i] II SPF40_1_4_p2[i] II
/*170-171-172*/	SPF4_1_6_p2[i] II SPF20_1_6_p2[i] II SPF40_1_6_p2[i] II

/*173-174-175*/	SPF4_2_2_m1[i] II SPF20_2_2_m1[i] II SPF40_2_2_m1[i] II
/*176-177-178*/	SPF4_2_4_m1[i] II SPF20_2_4_m1[i] II SPF40_2_4_m1[i] II
/*179-180-181*/	SPF4_2_6_m1[i] II SPF20_2_6_m1[i] II SPF40_2_6_m1[i] II
/*182-183-184*/	SPF4_2_2_p0[i] II SPF20_2_2_p0[i] II SPF40_2_2_p0[i] II
/*185-186-187*/	SPF4_2_4_p0[i] II SPF20_2_4_p0[i] II SPF40_2_4_p0[i] II
/*188-189-190*/	SPF4_2_6_p0[i] II SPF20_2_6_p0[i] II SPF40_2_6_p0[i] II
/*191-192-193*/	SPF4_2_2_p1[i] II SPF20_2_2_p1[i] II SPF40_2_2_p1[i] II
/*194-195-196*/	SPF4_2_4_p1[i] II SPF20_2_4_p1[i] II SPF40_2_4_p1[i] II
/*197-198-199*/	SPF4_2_6_p1[i] II SPF20_2_6_p1[i] II SPF40_2_6_p1[i] II
/*200-201-202*/	SPF4_2_2_p2[i] II SPF20_2_2_p2[i] II SPF40_2_2_p2[i] II
/*203-204-205*/	SPF4_2_4_p2[i] II SPF20_2_4_p2[i] II SPF40_2_4_p2[i] II
/*206-207-208*/	SPF4_2_6_p2[i] II SPF20_2_6_p2[i] II SPF40_2_6_p2[i] II




// Phonons Runge-Kutta 
/*209-210-211*/	SPR4_2_m1[i] II SPR20_2_m1[i] II SPR40_2_m1[i] II
/*212-213-214*/	SPR4_4_m1[i] II SPR20_4_m1[i] II SPR40_4_m1[i] II
/*215-216-217*/	SPR4_6_m1[i] II SPR20_6_m1[i] II SPR40_6_m1[i] II
/*218-219-220*/	SPR4_2_p0[i] II SPR20_2_p0[i] II SPR40_2_p0[i] II
/*221-222-223*/	SPR4_4_p0[i] II SPR20_4_p0[i] II SPR40_4_p0[i] II
/*224-225-226*/	SPR4_6_p0[i] II SPR20_6_p0[i] II SPR40_6_p0[i] II
/*227-228-229*/	SPR4_2_p1[i] II SPR20_2_p1[i] II SPR40_2_p1[i] II
/*230-231-232*/	SPR4_4_p1[i] II SPR20_4_p1[i] II SPR40_4_p1[i] II
/*233-234-235*/	SPR4_6_p1[i] II SPR20_6_p1[i] II SPR40_6_p1[i] II
/*236-237-238*/	SPR4_2_p2[i] II SPR20_2_p2[i] II SPR40_2_p2[i] II
/*239-240-241*/	SPR4_4_p2[i] II SPR20_4_p2[i] II SPR40_4_p2[i] II
/*242-243-244*/	SPR4_6_p2[i] II SPR20_6_p2[i] II SPR40_6_p2[i] II


// Fermions Runge-Kutta .// опять очень похоже
/*245-246-247*/	SFR4_0_m1[i] II SFR20_0_m1[i] II SFR40_0_m1[i] II
/*248-249-250*/	SFR4_1_m1[i] II SFR20_1_m1[i] II SFR40_1_m1[i] II
/*251-252-253*/	SFR4_2_m1[i] II SFR20_2_m1[i] II SFR40_2_m1[i] II
/*254-255-256*/	SFR4_0_p0[i] II SFR20_0_p0[i] II SFR40_0_p0[i] II
/*257-258-259*/	SFR4_1_p0[i] II SFR20_1_p0[i] II SFR40_1_p0[i] II
/*260-261-262*/	SFR4_2_p0[i] II SFR20_2_p0[i] II SFR40_2_p0[i] II
/*263-264-265*/	SFR4_0_p1[i] II SFR20_0_p1[i] II SFR40_0_p1[i] II
/*266-267-268*/	SFR4_1_p1[i] II SFR20_1_p1[i] II SFR40_1_p1[i] II
/*269-270-271*/	SFR4_2_p1[i] II SFR20_2_p1[i] II SFR40_2_p1[i] II
/*272-273-274*/	SFR4_0_p2[i] II SFR20_0_p2[i] II SFR40_0_p2[i] II
/*275-276-277*/	SFR4_1_p2[i] II SFR20_1_p2[i] II SFR40_1_p2[i] II
/*278-279-280*/ SFR4_2_p2[i] II SFR20_2_p2[i] II SFR40_2_p2[i] II

// Phonons+Fermions Runge-Kutta
/*281-282-283*/	SPFR4_0_2_m1[i] II SPFR20_0_2_m1[i] II SPFR40_0_2_m1[i] II
/*284-285-286*/	SPFR4_1_2_m1[i] II SPFR20_1_2_m1[i] II SPFR40_1_2_m1[i] II
/*287-288-289*/	SPFR4_2_2_m1[i] II SPFR20_2_2_m1[i] II SPFR40_2_2_m1[i] II
/*290-291-292*/	SPFR4_0_4_m1[i] II SPFR20_0_4_m1[i] II SPFR40_0_4_m1[i] II
/*293-294-295*/	SPFR4_1_4_m1[i] II SPFR20_1_4_m1[i] II SPFR40_1_4_m1[i] II
/*296-297-298*/	SPFR4_2_4_m1[i] II SPFR20_2_4_m1[i] II SPFR40_2_4_m1[i] II
/*299-300-301*/	SPFR4_0_6_m1[i] II SPFR20_0_6_m1[i] II SPFR40_0_6_m1[i] II
/*302-303-304*/	SPFR4_1_6_m1[i] II SPFR20_1_6_m1[i] II SPFR40_1_6_m1[i] II
/*305-306-307*/	SPFR4_2_6_m1[i] II SPFR20_2_6_m1[i] II SPFR40_2_6_m1[i] II

/*308-309-310*/	SPFR4_0_2_p0[i] II SPFR20_0_2_p0[i] II SPFR40_0_2_p0[i] II
/*311-312-313*/	SPFR4_1_2_p0[i] II SPFR20_1_2_p0[i] II SPFR40_1_2_p0[i] II
/*314-315-316*/	SPFR4_2_2_p0[i] II SPFR20_2_2_p0[i] II SPFR40_2_2_p0[i] II
/*317-318-319*/	SPFR4_0_4_p0[i] II SPFR20_0_4_p0[i] II SPFR40_0_4_p0[i] II
/*320-321-322*/	SPFR4_1_4_p0[i] II SPFR20_1_4_p0[i] II SPFR40_1_4_p0[i] II
/*323-324-325*/	SPFR4_2_4_p0[i] II SPFR20_2_4_p0[i] II SPFR40_2_4_p0[i] II
/*326-327-328*/	SPFR4_0_6_p0[i] II SPFR20_0_6_p0[i] II SPFR40_0_6_p0[i] II
/*329-330-331*/	SPFR4_1_6_p0[i] II SPFR20_1_6_p0[i] II SPFR40_1_6_p0[i] II
/*332-333-334*/	SPFR4_2_6_p0[i] II SPFR20_2_6_p0[i] II SPFR40_2_6_p0[i] II

/*335-336-337*/	SPFR4_0_2_p1[i] II SPFR20_0_2_p1[i] II SPFR40_0_2_p1[i] II
/*338-339-340*/	SPFR4_1_2_p1[i] II SPFR20_1_2_p1[i] II SPFR40_1_2_p1[i] II
/*341-342-343*/	SPFR4_2_2_p1[i] II SPFR20_2_2_p1[i] II SPFR40_2_2_p1[i] II
/*344-345-346*/	SPFR4_0_4_p1[i] II SPFR20_0_4_p1[i] II SPFR40_0_4_p1[i] II
/*347-348-349*/	SPFR4_1_4_p1[i] II SPFR20_1_4_p1[i] II SPFR40_1_4_p1[i] II
/*450-351-352*/	SPFR4_2_4_p1[i] II SPFR20_2_4_p1[i] II SPFR40_2_4_p1[i] II
/*354-355-356*/	SPFR4_0_6_p1[i] II SPFR20_0_6_p1[i] II SPFR40_0_6_p1[i] II
/*357-358-359*/	SPFR4_1_6_p1[i] II SPFR20_1_6_p1[i] II SPFR40_1_6_p1[i] II
/*340-341-342*/	SPFR4_2_6_p1[i] II SPFR20_2_6_p1[i] II SPFR40_2_6_p1[i] II

/*343-344-345*/	SPFR4_0_2_p2[i] II SPFR20_0_2_p2[i] II SPFR40_0_2_p2[i] II
/*346-347-348*/	SPFR4_1_2_p2[i] II SPFR20_1_2_p2[i] II SPFR40_1_2_p2[i] II
/*349-350-351*/	SPFR4_2_2_p2[i] II SPFR20_2_2_p2[i] II SPFR40_2_2_p2[i] II
/*352-353-354*/	SPFR4_0_4_p2[i] II SPFR20_0_4_p2[i] II SPFR40_0_4_p2[i] II
/*355-356-357*/	SPFR4_1_4_p2[i] II SPFR20_1_4_p2[i] II SPFR40_1_4_p2[i] II
/*358-359-360*/	SPFR4_2_4_p2[i] II SPFR20_2_4_p2[i] II SPFR40_2_4_p2[i] II
/*361-362-363*/	SPFR4_0_6_p2[i] II SPFR20_0_6_p2[i] II SPFR40_0_6_p2[i] II
/*364-365-366*/	SPFR4_1_6_p2[i] II SPFR20_1_6_p2[i] II SPFR40_1_6_p2[i] II
/*367-368-369*/	SPFR4_2_6_p2[i] II SPFR20_2_6_p2[i] II SPFR40_2_6_p2[i] II

//Imaginary// Неправильные индексы!!
//Simple Phonon
/*317-318-319*/	P4_2[i].imag() II P20_2[i].imag() II P40_2[i].imag() II
/*320-321-322*/	P4_4[i].imag() II P20_4[i].imag() II P40_4[i].imag() II
/*323-324-325*/	P4_6[i].imag() II P20_6[i].imag() II P40_6[i].imag() II//Simple Fermion
/*326-327-328*/	F4_0[i].imag() II F20_0[i].imag() II F40_0[i].imag() II
/*329-330-331*/	F4_1[i].imag() II F20_1[i].imag() II F40_1[i].imag() II
/*332-333-334*/	F4_2[i].imag() II F20_2[i].imag() II F40_2[i].imag() II//r Phonon
/*335-336-337*/	PR4_2[i].imag() II PR20_2[i].imag() II PR40_2[i].imag() II
/*338-339-340*/	PR4_4[i].imag() II PR20_4[i].imag() II PR40_4[i].imag() II
/*341-342-343*/	PR4_6[i].imag() II PR20_6[i].imag() II PR40_6[i].imag() II//r Fermion
/*344-345-346*/	FR4_0[i].imag() II FR20_0[i].imag() II FR40_0[i].imag() II
/*347-348-349*/	FR4_1[i].imag() II FR20_1[i].imag() II FR40_1[i].imag() II
/*450-351-352*/	FR4_2[i].imag() II FR20_2[i].imag() II FR40_2[i].imag() II// r Phonon + Fermion
/*353-354-355*/	PFR4_0_2[i].imag() II PFR20_0_2[i].imag() II PFR40_0_2[i].imag() II
/*356-357-358*/	PFR4_0_4[i].imag() II PFR20_0_4[i].imag() II PFR40_0_4[i].imag() II
/*359-360-361*/	PFR4_0_6[i].imag() II PFR20_0_6[i].imag() II PFR40_0_6[i].imag() II
/*362-363-364*/	PFR4_1_2[i].imag() II PFR20_1_2[i].imag() II PFR40_1_2[i].imag() II
/*365-366-367*/	PFR4_1_4[i].imag() II PFR20_1_4[i].imag() II PFR40_1_4[i].imag() II
/*368-369-370*/	PFR4_1_6[i].imag() II PFR20_1_6[i].imag() II PFR40_1_6[i].imag() II
/*371-372-373-*/PFR4_2_2[i].imag() II PFR20_2_2[i].imag() II PFR40_2_2[i].imag() II
/*374-375-376-*/PFR4_2_4[i].imag() II PFR20_2_4[i].imag() II PFR40_2_4[i].imag() II
/*377-378-379*/	PFR4_2_6[i].imag() II PFR20_2_6[i].imag() II PFR40_2_6[i].imag() II
	
'\n' ;
}
data.close();

return 0;
}

